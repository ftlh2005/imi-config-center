### Imi-Config-Center
安装：composer require phpben/imi-config-center<br><br>
基于Imi-Swoole的配置中心<br>
目前只支持阿里云产品ACM NACOS等<br>
如果有热心网友愿意提供配置中心服务器仅供测试的话感激不尽<br>
<br><br>
需要在config目录下新建website.php配置文件<br>
并且在config/config.php配置<br>

```
    'configs'    =>    [
        'beans'        =>    __DIR__.'/beans.php',
        'website'        =>    __DIR__.'/website.php',
    ],
```
所有配置都会动态修改到website配置中，你可以使用imi的Imi\Config::get("@app.website.xxx")获取配置<br>
或者使用config函数直接获取配置，例如 config("xxx")<br>
不管是文件还是阿里云其他驱动都如此，文件配置支持热更新修改
<br><br>
在beans配置增加相关配置：

```
    'ConfigCenter' => [
        // 同步配置间隔时间
        'interval' => 5,
        // 驱动类名
        'driver' => \Phpben\Imi\ConfigCenter\Driver\AliyunMseNacos::class,
        // 阿里云MSENacos配置
        'aliyun_mse_nacos' => [
            // endpoint
            'endpoint' => "mse-xxxxxx-p.nacos-ans.mse.aliyuncs.com",
            // SK
            'secret_key' => "",
            // AK
            'access_key' => "",
            // 实例ID
            'instance_id' => "",
            // DATA_ID
            'data_id' => "",
            // GROUPID
            'group' => "",
            // 命名空间
            'namespace' => "",
        ],
        // 阿里云ACM配置
        'aliyun_acm' => [
            // 命名空间
            'namespace' => "",
            // GROUPID
            'group' => "",
            // SK
            'secret_key' => "",
            // AK
            'access_key' => "",
            // endpoint
            'endpoint' => "acm.aliyun.com",
            // DATA_ID
            'data_id' => "",
        ],
        // FILE文件驱动配置
        'file' => [

        ],
    ],
```
