<?php

namespace Phpben\Imi\ConfigCenter\Listener;

use Imi\Config;
use Imi\Event\EventParam;
use Imi\Event\IEventListener;
use Imi\Bean\Annotation\Listener;
use Imi\Log\Log;

/**
 * @Listener("IMI.PIPE_MESSAGE.config_center")
 */
class PipeMessageConfigCenterListener implements IEventListener
{
    /**
     * 事件处理方法
     * @param EventParam $e
     * @return void
     */
    public function handle(EventParam $e): void
    {
        $config = $e->getData()['data']['configs'] ?? [];
        if ($config && $config !== Config::get('@app.website')) {
            Config::set("@app.website", $config);
            Log::info("[ConfigCenter] all config is updated");
        }
    }

}