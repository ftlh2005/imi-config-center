<?php

declare(strict_types=1);

namespace Phpben\Imi\ConfigCenter\Listener;

use Imi\App;
use Imi\Event\EventParam;
use Imi\Event\IEventListener;
use Imi\Bean\Annotation\Listener;
use Imi\Swoole\Process\ProcessManager;
use Imi\Log\Log;
use Imi\Config;

/**
 * @Listener(eventName="IMI.SERVERS.CREATE.AFTER")
 */
class ServerCreateListener implements IEventListener
{
    /**
     * 配置中心进程
     * @param EventParam $e
     */
    public function handle(EventParam $e): void
    {
        $config = Config::get('@app.beans.ConfigCenter');
        if ($config) {
            App::getBean('AutoRunProcessManager')->add('ConfigCenter', 'config_center');
            Log::info("Process [ConfigCenter] start");
        }
    }
}
