<?php
if (!function_exists("config")) {
    /**
     * 获取Website配置
     * @param string $name Website配置名称
     * @param mixed|null $default 默认值
     * @return mixed
     */
    function config(string $name = '', $default = null)
    {
        $config = "@app.website";
        $name && $config .= '.' . $name;
        return \Imi\Config::get($config, $default);
    }
}