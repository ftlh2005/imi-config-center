<?php

declare(strict_types=1);

return [
    'beanScan' => [
        'Phpben\Imi\ConfigCenter\Process',
        'Phpben\Imi\ConfigCenter\Listener',
    ],
];